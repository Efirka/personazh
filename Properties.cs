﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Personazh
{
    public class Properties
    {
        string rassa;
        string color;
        string stat;
        string names;
        bool hvost;

        string Active { get; }

        public void SetHaract()
        {
            switch ("Расса: " + rassa)
            {
                case "1":
                    {
                        Console.WriteLine("Человек");
                        break;
                    }
                case "2":
                    {
                        Console.WriteLine("Обротень");
                        break;
                    }
                case "3":
                    {
                        Console.WriteLine("Дреней");
                        break;
                    }
                case "4":
                    {
                        Console.WriteLine("Эльф");
                        break;
                    }
                default:
                    break;
            }

            Console.WriteLine("Пол: " + stat);
            if (hvost == true) Console.WriteLine("Есть хвост");
            else { Console.WriteLine("Нет хвоста"); }
            Console.WriteLine("Цвет: " + color);

            Console.WriteLine("Активен: " + Active);

        }

        public void Krasuetsja()
        {
            Console.Clear();
            Console.WriteLine(names + " красуется");
        }

        public void GotBoj()
        {
            Console.WriteLine(names + " готовится к бою");
        }

        public void Ataka()
        {
            Console.WriteLine(names + " атакует");
        }

        public Properties(string name, string rase)
        {
            Active = "Да";
            names = name;
            rassa = rase;
            stat = "Пол не определен";
            switch (rassa)
            {
                case "1":
                    {
                        hvost = false;
                        color = "Белый";
                        break;
                    }
                case "2":
                    {
                        hvost = true;
                        color = "Серый";
                        break;
                    }
                case "3":
                    {
                        hvost = true;
                        color = "Фиолетовый";
                        break;
                    }
                case "4":
                    {
                        hvost = false;
                        color = "Зеленый";
                        break;
                    }
                default:
                    break;
            }



        }
        public Properties(string name, string rase, string pol) : this(name, rase)
        {
            stat = pol;


        }
        public Properties(string name, string rase, string pol, string colore) : this(name, rase, pol)
        {
            color = colore;

        }

    }
}
